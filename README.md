# File structure

## HTML
---

`index.html` -- homepage

`about.html` -- TODO create

#### Graph pages: HTML + Javascript
---
`overview.html` -- Articles and authors overview

`graph-overview.js`

---


`temporal-evolution.html` -- Temporal evolution (TEMP FILE)

`graph-temporal-evolution.js`

---

`community-detection-articles.html` -- Community detection: Articles on similar topics

`graph-community-detection-articles.js`

---

`community-detection-authors.html` -- Community detection: Authors who work on similar topics TODO create

`graph-community-detection-authors.js`

---

`ranking-articles.html` -- Ranking of the most important articles

`graph-ranking-articles.html`

---

`collaboration-prediction.html` -- Collaborations Prediction for Authors

`graph-collaboration-prediction.js`



## CSS
---
`styles.css` -- Imports of all stylesheets USE THIS ONE WHEN YOU DON'T KNOW WHERE TO PUT STUFF

`base-styles.css` -- Base styles such as typography and buttons

`navigation.css` -- Styles for the navigation bars and the offscreen navigation

`homepage.ss` -- Styles for the homepage

`graphs.css` -- Styles for the graphs' canvases



## JavaScript
---
`search-bar.js` -- Code for search function in graph pages

`canvas-expansion.js` -- Code to allow for canvas expansion on click in graph pages


`particles.js` -- Base file for homepage animation

`particles-config.js` -- Configuration file for particles.js
