//
// È QUELLO DELLA COMMUNITY DETECTION
// TODO: MODIFICARE
//

// instantiate sigma
var s = new sigma({
  renderers: [{
    container: 'sigma-canvas',
    type: 'canvas'
  }],
  settings: {
    labelThreshold: 666,
    mouseInertiaDuration: 0,
    touchInertiaDuration: 0,
    dragTimeout: 0,
    mouseZoomDuration: 0,
    zoomingRatio: 1.1,
    batchEdgesDrawing: true,
    canvasEdgesBatchSize: 250,
    mouseWheelEnabled: false
    //maxNodeSize: 4
  }
});

//var originalColors = [];



// load nodes, edges and render
sigma.parsers.json(
  'js/assets/titles.json',
  s,
  function() {

    var edges = s.graph.edges();

    //Using for loop
    for (edge in edges){
        //set curve type of link
        edges[edge].type = 'curve';

        //extract color
        var color = edges[edge].color.match(/\d+/g);

        color = color.map( val => val-20>=0?val-20:0 );

        //set transparency
        edges[edge].color = "rgba("+color[0]+","+color[1]+","+color[2]+",0.1)";

    }

    s.refresh();
  }
);
