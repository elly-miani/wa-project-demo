$(document).ready(function(){

  // vieport width to determine layout
  var $viewport = $(window).width();
  // expand-icon div
  var $overlay = $(".expand-icon");
  // left column: canvas that will be expanded
  var $canva = $("#graph-preview");
  // right column: description that will go under
  var $overview = $("#overview");
  // button that needs to be hidden when canva is expanded / when on a single column layout
  var $learnMoreButton = $("#overview a");




  // CANVAS RESIZE
  // bind function to window's resize
  $(window).on("resize", checkViewportWidth);

  function checkViewportWidth() {
    $viewport = $(window).width();

    // if window is resized to a single column layout: hide button and expand icon
    if($viewport < 960) {
      $overlay.addClass("uk-hidden");
      $learnMoreButton.addClass("uk-hidden");
    }

    // if window is resized to a two columns layout: show button and expand icon
    if($viewport > 960) {
      $overlay.removeClass("uk-hidden");
      $learnMoreButton.removeClass("uk-hidden");
    }
  }

  // when canva is clicked
  $("#canvas").click( function () {
      // remove expand icon
      $overlay.remove();
      // remove learn more button
      $learnMoreButton.remove();

      // expand canva to full width
      $canva.addClass("uk-width-1-1@m");
      $canva.removeClass("uk-width-3-5@m");

      // expand overview to full width (goes under)
      $overview.addClass("uk-width-1-1@m");
      $overview.removeClass("uk-width-2-5@m");

      // $searchbar.parent().parent().removeClass("uk-hidden");

      //center sigma's camera again
      s.camera.goTo({x:0,y:0});

      //add an event listener for the transition
      $overview.on('transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd',
        function(){
          s.refresh();
          s.settings('mouseWheelEnabled', true);
        }
      );
  });
});
