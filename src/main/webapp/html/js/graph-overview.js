//
// TODO: MODIFICARE PER IL GRAFICO DELL'OVERVIEW -- IL CORRENTE È QUELLO DELLA COMMUNITY DETECTION
//

// instantiate sigma
var s = new sigma({
  renderers: [{
    container: 'sigma-canvas',
    type: 'canvas'
  }],
  settings: {
    labelThreshold: 666,
    mouseInertiaDuration: 0,
    touchInertiaDuration: 0,
    dragTimeout: 0,
    mouseZoomDuration: 0,
    zoomingRatio: 1.1,
    batchEdgesDrawing: true,
    canvasEdgesBatchSize: 250,
    mouseWheelEnabled: false
    //maxNodeSize: 4
  }
});

//var originalColors = [];

//initialize the variables for
//storing the network and for the
//loading checks
var authorNames;
var authorNamesBuild = false;
var articleTitles;
var articleTitlesBuilt = false;
var edgeList;
var edgeListBuilt = false;
var N1 = 0;
var N2 = 0;
//load the authors
$.getJSON("js/new_assets/authorNames.json")
  .done(function(data){
      authorNames = data;
      N1 = authorNames.length;
      authorNamesBuild = true;
      buildNetwork();
    })
  .fail(function() {
    console.log( "error when loading the authors JSON" );
    }
  );
//load the articles
$.getJSON("js/new_assets/articleTitles.json")
  .done(function(data){
      articleTitles = data;
      N2 = articleTitles.length;
      articleTitlesBuilt = true;
      buildNetwork();
    })
  .fail(function() {
    console.log( "error when loading the articles JSON" );
    }
  );
//load the actual network
$.getJSON("js/new_assets/globalEdgeList.json")
  .done(function(data){
      edgeList = data;
      edgeListBuilt = true;
      buildNetwork();
    })
  .fail(function() {
    console.log( "error when loading the articles JSON" );
    }
  );

function buildNetwork(){
  if(authorNamesBuild && articleTitlesBuilt && edgeListBuilt){
    //now we have all the necessary data!
    console.log("building started");

    var authors_radius = 100, articles_radius = 300;
    for(var i = 0; i<N1; i++){
      // x coord = articles_radius*cos(2*i*Math.PI/N1)
      // y coord = articles_radius*sin(2*i*Math.PI/N1)
    }
    for(var i = 0; i<N2; i++){
      // x coord = authors_radius*cos(2*i*Math.PI/N2)
      // y coord = authors_radius*sin(2*i*Math.PI/N2)
    }

  }
}
