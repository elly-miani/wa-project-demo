// instantiate sigma
var s_edges = new sigma({
  renderers: [{
    container: 'sigma-canvas',
    type: 'canvas'
  }],
  settings: {
    drawLabels: false,
    defaultEdgeType: 'curve',
    minNodeSize: 0,
    maxNodeSize: 0,
    drawNodes: false,
    enableCamera: false
  }
});

var s_nodes = new sigma({
  renderers: [{
    container: 'sigma-canvas',
    type: 'canvas'
  }],
  settings: {
    drawLabels: false,
    defaultEdgeType: 'curve',
    minNodeSize: 0,
    maxNodeSize: 0,
    animationsTime: 500,
    drawEdges: false,
    enableCamera: false,
    rescaleIgnoreSize: true // important to prevent not desired node resize
  }
});

// initialize the fake hub array
var hubs = [];

// rgba alpha channel value
var alpha = 0.05;

sigma.parsers.json(
  'js/assets/titles.json',
  s_nodes,
  function(){
    // set proper colors
    var nodes = s_nodes.graph.nodes();
    nodes.forEach(node => {
      var originalColor = node.color;
      var rgb = originalColor.match(/\d+/g);
      var startingColor = "rgba(" + rgb[0] + "," + rgb[1] + "," + rgb[2] + "," + alpha + ")";
      node.color = startingColor;
      node.normalColor = startingColor;
      node.highlightColor = originalColor;
    });

    // fake hubs used in order to test the animation
    for (var i = 0; i < 10; i++) {
      node = nodes[i];
      hubs[i] = node.id;
      originalSize = node.size;
      node.normalSize = originalSize;
      node.highlightSize = 5 * originalSize;
    }

    // sigma refresh
    s_nodes.refresh();
});

sigma.parsers.json(
  'js/assets/titles.json',
  s_edges,
  function(){
    s_edges.graph.edges().forEach(edge => {
      var rgb = edge.color.match(/\d+/g);
      edge.color = "rgba(" + rgb[0] + "," + rgb[1] + "," + rgb[2] + "," + alpha + ")";
    });

    // sigma refresh
    s_edges.refresh();
  }
);

// animation function
var status = 0;
function animate() {
  if (status == 0) {
    status = 1;
    sigma.plugins.animate(
      s_nodes,
      {
        color: 'highlightColor',
        size: 'highlightSize'
      },
      {
        nodes: hubs
      }
    );
  }
  else {
    status = 0;
    sigma.plugins.animate(
      s_nodes,
      {
        color: 'normalColor',
        size: 'normalSize'
      },
      {
        nodes: hubs
      }
    );
  }
}

// bind animations events
$('#canvas').bind('mouseenter', function() {
  animate();
}).bind('mouseleave', function() {
  animate();
});
