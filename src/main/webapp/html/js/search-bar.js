$(document).ready(function(){

  var $searchbar = $("#graph-search-bar");
  var $coolofftime = 150;
  var $inCoolOff = false;

  // SEARCH BAR
  $searchbar.on('input',function(e){
    if(!$inCoolOff){
      $inCoolOff = true;
      setTimeout(function(){if($inCoolOff){$inCoolOff=false;}}, $coolofftime);
      //console.log($searchbar.val());
      //the replace is to sanitize the input
      matchNodes($searchbar.val().replace(/<(|\/|[^>\/bi]|\/[^>bi]|[^\/>][^>]+|\/[^>][^>]+)>/g, ''));
    }
  });


  // SEARCH FUNCTION FOR SEARCH BAR
  function matchNodes(text){

    //lower case for case insentitive search
    var texts = text.toLowerCase().split(" ");

    s.graph.nodes().forEach(function(node){

      if(typeof node.label !== 'undefined'){
        var found = true;

        for(text in texts){
          found = found && node.label.toLowerCase().search(texts[text]) >= 0;

          //exit as soon as the flag is set to false
          if(!found){
            break;
          }
        }

        if(!found){
          node.hidden = true;
        }else{
          if(node.hidden){
            node.hidden = false;
          }
        }
      }else{
        // fix for nodes without labels
        if(text.length>0){
          node.hidden = true;
        }else{
          if(node.hidden){
            node.hidden = false;
          }
        }
      }

    });

    s.refresh();

  }

});
